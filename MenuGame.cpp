/* The MIT License (MIT)
 * Copyright (c) 2012 Vittorio Romeo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <SFML/Audio.hpp>
#include <SSVStart.h>
#include "Data/StyleData.h"
#include "Global/Assets.h"
#include "Global/Config.h"
#include "Utils/Utils.h"
#include "MenuGame.h"

namespace hg
{
	MenuGame::MenuGame(GameWindow& mGameWindow) : window(mGameWindow)
	{
		recreateTextures();

		game.addUpdateFunc([&](float frameTime) { update(frameTime); });
		game.addDrawFunc([&]{ draw(); }, 0);

		levelDataIds = getAllMenuLevelDataIds();
		setIndex(0);
	}

	void MenuGame::recreateTextures()
	{		
		gameTexture.create(getSizeX(), getSizeY(), 32);
		gameTexture.setView(View{Vector2f{0,0}, Vector2f{getSizeX() * getZoomFactor(), getSizeY() * getZoomFactor()}});
		gameTexture.setSmooth(true);
		gameSprite.setTexture(gameTexture.getTexture(), false);
		gameSprite.setOrigin(getSizeX()/ 2, getSizeY()/ 2);
		gameSprite.setPosition(window.getWidth() / 2, window.getHeight() / 2);

		menuTexture.create(getSizeX(), getSizeY(), 32);
		menuTexture.setView(View{Vector2f(window.getWidth()/2, 810/2), Vector2f{getSizeX() * getZoomFactor(), getSizeY() * getZoomFactor()}});
		menuTexture.setSmooth(true);
		menuSprite.setTexture(menuTexture.getTexture(), false);
		menuSprite.setOrigin(getSizeX()/ 2, getSizeY()/ 2);
		menuSprite.setPosition(getWidth() / 2, getHeight() / 2);
	}

	void MenuGame::init()
	{
		stopAllMusic();
		stopAllSounds();

		playSound("open_hexagon");
	}

	void MenuGame::setIndex(int mIndex)
	{
		currentIndex = mIndex;

		if(currentIndex > (int)(levelDataIds.size() - 1)) currentIndex = 0;
		else if(currentIndex < 0) currentIndex = levelDataIds.size() - 1;

		levelData = getLevelData(levelDataIds[currentIndex]);
		styleData = getStyleData(levelData.getStyleId());
	}

	void MenuGame::update(float mFrameTime)
	{
		styleData.update(mFrameTime);
		gameSprite.rotate(levelData.getRotationSpeed() * 10 * mFrameTime);

		if(inputDelay <= 0)
		{
			if(window.isKeyPressed(Keyboard::LAlt) && window.isKeyPressed(Keyboard::Return))
			{
				setFullscreen(window, !window.getFullscreen());
				recreateTextures();
				hgPtr->recreateTextures();
				inputDelay = 25;
			}
			else if(window.isKeyPressed(Keyboard::Escape)) inputDelay = 25;
			else if(window.isKeyPressed(Keyboard::Right))
			{
				playSound("beep");
				setIndex(currentIndex + 1);

				inputDelay = 14;
			}
			else if(window.isKeyPressed(Keyboard::Left))
			{
				playSound("beep");
				setIndex(currentIndex - 1);

				inputDelay = 14;
			}
			else if(window.isKeyPressed(Keyboard::Return))
			{
				playSound("beep");
				window.setGame(&hgPtr->getGame());
				hgPtr->newGame(levelDataIds[currentIndex], true);

				inputDelay = 14;
			}			
		}
		else
		{
			inputDelay -= 1 * mFrameTime;

			if(inputDelay < 1.0f && window.isKeyPressed(Keyboard::Escape)) window.stop();
		}
	}
	void MenuGame::draw()
	{
		gameTexture.clear(styleData.getCurrentA());
		menuTexture.clear(Color{0,0,0,0});

		drawBackground();
		drawText();

		gameTexture.display();
		menuTexture.display();

		drawOnWindow(gameSprite);
		drawOnWindow(menuSprite);
	}
	void MenuGame::drawBackground()
	{
		Vector2f centerPos{0,0};
		int sides{6};
		float div{360.f / sides * 1.0001f};
		float distance{1500};

		VertexArray vertices{PrimitiveType::Triangles, 3};

		for(int i {0}; i < sides; i++)
		{
			float angle { div * i };
			Color currentColor { styleData.getCurrentA() };

			if (i % 2 == 0)
			{
				currentColor = styleData.getCurrentB();
				if (i == sides - 1) currentColor = getColorDarkened(currentColor, 1.4f);
			}

			Vector2f p1 = getOrbit(centerPos, angle + div * 0.5f, distance);
			Vector2f p2 = getOrbit(centerPos, angle - div * 0.5f, distance);

			vertices.append(Vertex{centerPos, currentColor});
			vertices.append(Vertex{p1, currentColor});
			vertices.append(Vertex{p2, currentColor});
		}

		gameTexture.draw(vertices);
	}
	void MenuGame::drawText()
	{
		Color mainColor{styleData.getCurrentMain()};

		title1.setOrigin(title1.getGlobalBounds().width / 2, 0);
		title1.setStyle(Text::Bold);
		title1.setColor(mainColor);
		title1.setPosition(window.getWidth() / 2, 45);
		drawOnMenuTexture(title1);

		title2.setOrigin(title2.getGlobalBounds().width / 2, 0);
		title2.setStyle(Text::Bold);
		title2.setColor(mainColor);
		title2.setPosition(window.getWidth() / 2, 80);
		drawOnMenuTexture(title2);

		title3.setOrigin(title3.getGlobalBounds().width / 2, 0);
		title3.setStyle(Text::Bold);
		title3.setColor(mainColor);
		title3.setPosition(window.getWidth() / 2, 240);
		drawOnMenuTexture(title3);

		title4.setOrigin(title4.getGlobalBounds().width / 2, 0);
		title4.setStyle(Text::Bold);		
		title4.setColor(mainColor);
		title4.setPosition(window.getWidth() / 2, 270);
		drawOnMenuTexture(title4);

		levelTime.setString("best time: " + toStr(getScore(levelData.getId())));
		levelTime.setOrigin(levelTime.getGlobalBounds().width / 2, 0);
		levelTime.setColor(mainColor);
		levelTime.setPosition(window.getWidth() / 2, 768 - 425);
		drawOnMenuTexture(levelTime);

		cProfText.setString("current profile: " + getCurrentProfile().getName());
		cProfText.setOrigin(cProfText.getGlobalBounds().width / 2, 0);
		cProfText.setColor(mainColor);
		cProfText.setPosition(window.getWidth() / 2, 768 - 375);
		drawOnMenuTexture(cProfText);

		levelName.setString(levelData.getName());
		levelName.setOrigin(levelName.getGlobalBounds().width / 2, 0);
		levelName.setColor(mainColor);
		levelName.setPosition(window.getWidth() / 2, 768 - 295 - 40*(countNewLines(levelData.getName())));
		drawOnMenuTexture(levelName);

		levelDesc.setString(levelData.getDescription());
		levelDesc.setOrigin(levelDesc.getGlobalBounds().width / 2, 0);
		levelDesc.setColor(mainColor);
		levelDesc.setPosition(window.getWidth() / 2, 768 - 220 + 25*(countNewLines(levelData.getName())) );
		drawOnMenuTexture(levelDesc);

		levelAuth.setString("author: " + levelData.getAuthor());
		levelAuth.setOrigin(levelAuth.getGlobalBounds().width / 2, 0);
		levelAuth.setColor(mainColor);
		levelAuth.setPosition(window.getWidth() / 2, 768 - 75);
		drawOnMenuTexture(levelAuth);

		MusicData musicData{getMusicData(levelData.getMusicId())};

		levelMusc.setString("music: " + musicData.getName() + " by " + musicData.getAuthor() + " (" + musicData.getAlbum() + ")");
		levelMusc.setOrigin(levelMusc.getGlobalBounds().width / 2, 0);
		levelMusc.setColor(mainColor);
		levelMusc.setPosition(window.getWidth() / 2, 768 - 60);
		drawOnMenuTexture(levelMusc);
	}
	
	void MenuGame::drawOnGameTexture(Drawable &mDrawable) { gameTexture.draw(mDrawable); }
	void MenuGame::drawOnMenuTexture(Drawable &mDrawable) { menuTexture.draw(mDrawable); }
	void MenuGame::drawOnWindow(Drawable &mDrawable) { window.renderWindow.draw(mDrawable); }

	Game& MenuGame::getGame() { return game; }
}

